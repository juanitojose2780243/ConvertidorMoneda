package lab2.facci.juanalcivar.convertidormoneda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText editTextValorDolar;
    EditText editTextValorEuro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextValorDolar = (EditText) findViewById(R.id.editTextValorDolar);
        editTextValorEuro = (EditText) findViewById(R.id.editTextValorEuro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
        //Llamar activity que mueste el resultado
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra("valor", editTextValorDolar.getText().toString());
                Log.e("Dato enviado", editTextValorDolar.getText().toString());
                startActivity(intent);

                Intent intent1 = new Intent(MainActivity.this, Result2Activity.class);
                intent1.putExtra("valor2", editTextValorEuro.getText().toString());
                Log.e("Dato enviado", editTextValorEuro.getText().toString());
                startActivity(intent1);
            }
        });
    }

}
