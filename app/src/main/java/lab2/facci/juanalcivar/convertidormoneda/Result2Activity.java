package lab2.facci.juanalcivar.convertidormoneda;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
public class Result2Activity extends AppCompatActivity {

    TextView textViewResultado2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result2);


        textViewResultado2 = (TextView) findViewById(R.id.textViewResultado2);
        String dato2 = getIntent().getExtras().getString("valor2");
        Double euro = Double.parseDouble(dato2);
        Double resultado2 = euro * 1.19;
        Log.e("Dato recibido", dato2);
        textViewResultado2.setText(resultado2.toString());
    }
}
