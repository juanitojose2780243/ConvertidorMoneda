package lab2.facci.juanalcivar.convertidormoneda;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView textViewResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);



        textViewResultado = (TextView) findViewById(R.id.textViewResultado);
        //String dato = getIntent().getStringExtra("valor");
        //Valor del euro equivale a 0,83
        String dato = getIntent().getExtras().getString("valor");
        Double dolar = Double.parseDouble(dato);
        Double resultado = dolar * 0.8370;
        Log.e("Dato recibido", dato);
        textViewResultado.setText(resultado.toString());




    }
}
